<?php
class M_santri extends CI_Model{
	function cek_login($table,$where){
        return $this->db->get_where($table,$where);
	}
	function orang (){
        $query = $this->db->get('santri');
        return $query->result_array();
    }
	
	function show_santri(){
		$hasil=$this->db->query("SELECT * FROM santri");
		return $hasil;
	}

	function tambah_hafalan($id_santri,$nama_santri,$surah,$ayat){
		$hasil=$this->db->query("INSERT INTO santri (id_santri,nama_santri,surah,ayat) VALUES ('$id_santri','$nama_santri','$surah','$ayat')");
		return $hasil;
	}

	function edit_hafalan($id_santri,$nama_santri,$surah,$ayat){
		$hasil=$this->db->query("UPDATE santri SET nama_santri='$nama_santri',surah='$surah',ayat='$ayat' WHERE id_santri='$id_santri'");
		return $hasil;
	}

	function hapus_hafalan($id_santri){
		$hasil=$this->db->query("DELETE FROM santri WHERE id_santri='$id_santri'");
		return $hasil;
	}
	
}