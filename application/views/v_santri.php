<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="utf-8">
	<title>Tabungan Akhirat</title>
	<link href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
</head>
<body>
<div class="container rounded shadow py-0 m-2" id="main" style="width:calc(100% - 200px)">
    <div class="navbar navbar-expand-lg navbar-dark bg-dark rounded shadow p-1 m-0" >
      </a>
      <div class="mr-sm-2 navbar-nav ml-auto" style="float:right">
        <a href="<?php echo base_url().'index.php/santri/logout'?>" style="color:black"><h5><i class="fa fa-users"></i> Logout</h5></a>
      </div>
<div class="container">
	<h1>Tabungan Akhirat</small>
		<div class="pull-right"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Add New</a></div>		
	</h1>

	<table class="table table-bordered table-striped" id="mydata">
		<thead>
			<tr>
				<td>ID Santri</td>
				<td>Nama Santri</td>
				<td>surah</td>
				<td>Ayat</td>
                <td>Aksi</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				foreach($data->result_array() as $i):
					$id_santri=$i['id_santri'];
					$nama_santri=$i['nama_santri'];
					$surah=$i['surah'];
					$ayat=$i['ayat'];
			?>
			<tr>
				<td><?php echo $id_santri;?></td>
				<td><?php echo $nama_santri;?></td>
				<td><?php echo $surah;?></td>
				<td><?php echo $ayat;?></td>
                <td style="width: 120px;">
                    <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_edit<?php echo $id_santri;?>"> Edit</a>
                    <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal_hapus<?php echo $id_santri;?>"> Hapus</a>
                </td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	
</div>


    <!-- ============ MODAL ADD SANTRI =============== -->
        <div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Tabungan</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/santri/tambah_hafalan'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-xs-3" >ID Santri</label>
                        <div class="col-xs-8">
                            <input name="id_santri" class="form-control" type="text" placeholder="ID Santri" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Santri</label>
                        <div class="col-xs-8">
                            <input name="nama_santri" class="form-control" type="text" placeholder="Nama Santri" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >surah</label>
                        <div class="col-xs-8">
                            <input name="surah" class="form-control" type="text" placeholder="surah" required>
                             </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Ayat</label>
                        <div class="col-xs-8">
                            <input name="ayat" class="form-control" type="text" placeholder="ayat" required>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>
    <!--END MODAL ADD SANTRI-->

    <!-- ============ MODAL EDIT SANTRI =============== -->
    <?php 
        foreach($data->result_array() as $i):
            $id_santri=$i['id_santri'];
            $nama_santri=$i['nama_santri'];
            $surah=$i['surah'];
            $ayat=$i['ayat'];
        ?>
        <div class="modal fade" id="modal_edit<?php echo $id_santri;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/santri/edit_hafalan'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-xs-3" >ID Santri</label>
                        <div class="col-xs-8">
                            <input name="id_santri" value="<?php echo $id_santri;?>" class="form-control" type="text" placeholder="ID Santri" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Santri</label>
                        <div class="col-xs-8">
                            <input name="nama_santri" value="<?php echo $nama_santri;?>" class="form-control" type="text" placeholder="Nama Santri" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >surah</label>
                        <div class="col-xs-8">
                        <input name="surah" value="<?php echo $surah;?>" class="form-control" type="text" placeholder="surah" required>
                             </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Ayat</label>
                        <div class="col-xs-8">
                        <input name="ayat" value="<?php echo $ayat;?>" class="form-control" type="text" placeholder="ayat" required>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Update</button>
                </div>
            </form>
            </div>
            </div>
        </div>

    <?php endforeach;?>
    <!--END MODAL EDIT SANTRI-->

    <?php 
        foreach($data->result_array() as $i):
            $id_santri=$i['id_santri'];
            $nama_santri=$i['nama_santri'];
            $surah=$i['surah'];
            $ayat=$i['ayat'];
        ?>
     <!-- ============ MODAL HAPUS SANTRI =============== -->
        <div class="modal fade" id="modal_hapus<?php echo $id_santri;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Hapus Data</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/santri/hapus_hafalan'?>">
                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $nama_santri;?></b></p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_santri" value="<?php echo $id_santri;?>">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
    <?php endforeach;?>
    <!--END MODAL HAPUS SANTRI-->

<script src="<?php echo base_url().'assets/js/jquery-2.2.4.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/moment.js'?>"></script>
<script>
	$(document).ready(function(){
		$('#mydata').DataTable();
	});
</script>
</body>
</html>