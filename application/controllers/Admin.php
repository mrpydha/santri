<?php

class Admin extends CI_Controller{

    function __construct(){
        parent::__construct();

        if($this->session->userdata('status') != "login"){
            redirect(base_url("index.php/Santri"));
        }
    }

    function index(){
        $x['data']=$this->m_santri->show_santri();
		$this->load->view('v_santri',$x);
        // $data=array(
        //     'data' => $this->M_santri->orang()
        // );
        // $this->load->view('v_admin',$data);
    }
	function homepage(){
		$this->load->view('home');
	}
}
