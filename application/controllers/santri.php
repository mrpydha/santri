<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Santri extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_santri');
	}
	
	function index(){

				$this->load->view('home');

		// $x['data']=$this->m_barang->show_santri();
		// $this->load->view('v_santri',$x);

	}
	function aksi_login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
				$query = $this->db->get('user');
        $where = array(
            'username' => $username,
            'password' => $password
            );
        $cek = $this->m_santri->cek_login("user",$where)->num_rows();
        if($cek > 0){
					$row = $query->row();
            $data_session = array(
                // 'nama' => $username,
								
				// 				'surah' => $row->surah,
				// 				'ayat' => $row->ayat,
                'status' => "login"
                );

            $this->session->set_userdata($data_session);

            redirect(base_url("index.php/santri/login"));

        }else{
            echo "Username dan password salah !";
        }
	}
	function login(){
        $x['data']=$this->m_santri->show_santri();
		$this->load->view('v_santri',$x);
        // $data=array(
        //     'data' => $this->M_santri->orang()
        // );
        // $this->load->view('v_admin',$data);
    }

	function tambah_hafalan(){
		$id_santri=$this->input->post('id_santri');
		$nama_santri=$this->input->post('nama_santri');
		$surah=$this->input->post('surah');
		$ayat=$this->input->post('ayat');
		$this->m_santri->tambah_hafalan($id_santri,$nama_santri,$surah,$ayat);
		redirect('santri/login');
	}

	function edit_hafalan(){
		$id_santri=$this->input->post('id_santri');
		$nama_santri=$this->input->post('nama_santri');
		$surah=$this->input->post('surah');
		$ayat=$this->input->post('ayat');
		$this->m_santri->edit_hafalan($id_santri,$nama_santri,$surah,$ayat);
		redirect('santri/login');
	}

	function hapus_hafalan(){
		$id_santri=$this->input->post('id_santri');
		$this->m_santri->hapus_hafalan($id_santri);
		redirect('santri/login');
	}

	function logout(){
        $this->session->sess_destroy();
		redirect(base_url());
    }
}
